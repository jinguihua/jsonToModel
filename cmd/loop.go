package cmd

import (
	"fmt"
	"gitee.com/any-call/gobase/frame/myctrl"
	"gitee.com/any-call/gobase/util/mycmd"
	"github.com/spf13/cobra"
	"os"
	"time"
)

var (
	duration int64
	command  string
)

func init() {
	rootCmd.AddCommand(loopCmd)

	loopCmd.PersistentFlags().Int64Var(&duration, "duration", 1, "请输入duration(s)")
	loopCmd.PersistentFlags().StringVar(&command, "cmd", "", "请输入命令")
}

var loopCmd = &cobra.Command{
	Use:   "loop",
	Short: "loop 指定的command",
	Long:  `loop command `,
	Run: func(cmd *cobra.Command, args []string) {
		if command == "" {
			fmt.Fprintln(os.Stderr, "empty loop cmd")
			os.Exit(1)
		}

		myctrl.TimerExec(time.Duration(time.Second*time.Duration(duration)), func() {
			go func() {
				if output, err := mycmd.Execbash(command); err != nil {
					fmt.Printf("%s : %s \r\n", time.Now().Format("2006-01-02 15:04:05"), err.Error())
				} else {
					fmt.Printf("%s : %s \r\n", time.Now().Format("2006-01-02 15:04:05"), output)
				}
			}()
		})

		os.Exit(0)
	},
}
